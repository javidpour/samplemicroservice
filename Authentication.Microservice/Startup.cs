using Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Authentication.Microservice
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //===================================================
            services.AddControllers();
            //===================================================
            services.AddTransient<HttpClient>();
            //===================================================
            services.AddCustomSwagger();
            //===================================================
            services.AddCustomDbContext(Configuration);
            //===================================================
            services.AddCustomMediatR();
            //===================================================
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //===================================================
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //===================================================
            app.UseCustomSwagger();
            //===================================================
            app.UseRouting();
            //===================================================
            //app.UseAuthorization();
            //===================================================
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //===================================================
        }
    }
}
