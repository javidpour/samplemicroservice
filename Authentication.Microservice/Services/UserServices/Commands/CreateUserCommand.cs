﻿using MediatR;
using Persistence;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Services.UserServices.Commands
{
    public class CreateUserCommand : IRequest<int>
    {
        //===================================================
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }
        //===================================================
        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        //===================================================
        /// <summary>
        /// آدرس
        /// </summary>
        public string Address { get; set; }
        //===================================================
        /// <summary>
        /// شماره تلفن همراه
        /// </summary>
        public string MobileNumber { get; set; }
        //===================================================
        /// <summary>
        /// پست اکترونیکی
        /// </summary>
        public string Email { get; set; }
        //===================================================

        public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
        {
            //===================================================
            private readonly IApplicationDbContext _context;
            private readonly HttpClient _httpClient;

            //===================================================
            public CreateUserCommandHandler(IApplicationDbContext context, HttpClient httpClient)
            {
                _context = context;
                _httpClient = httpClient;
            }
            //===================================================
            public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
            {
                //===================================================
                // بهتر است در اینگونه موارد از 
                // automapper
                // استفاده شود
                var user = new Domain.User
                {
                    MobileNumber = request.MobileNumber,
                    Email = request.Email,
                };
                //===================================================
                _context.Users.Add(user);
                await _context.SaveChanges();
                //===================================================
                //Create userProfile
                // در این بخش بهترین راه استفاده الگوی ساگا برای کنترل تراکنش های توزیع شده
                // میباشد.که میتوان با استفاده از ابزاری مانند
                // rabbitMQ
                // این الگو را پیاده سازی نمود
                // البته در اینجا بنده به صورت دستی کنترل هایی را انجام دادم
                var userProfile = new Dtos.UserProfileDto
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Address = request.Address,
                    UserId = user.Id
                };
                var dataJson = JsonSerializer.Serialize(userProfile);
                var stringContent = new StringContent(dataJson, Encoding.UTF8, mediaType: "application/json");
                // میبایست آدرس سرویس ها را تا جای ممکن در فایل کانفیگ ذخیره و نگهداری نمود
                var result = await _httpClient.PostAsync("http://localhost:48002/User", stringContent);
                if(result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    // رول بک کردن عملیات ثبت کاربر
                    _context.Users.Remove(user);
                    await _context.SaveChanges();
                    return 0;
                }
                //===================================================
                return user.Id;
            }
        }
    }
}
