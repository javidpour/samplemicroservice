﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Services.UserServices.Queries
{
    public class GetUserByIdQuery : IRequest<Dtos.UserInformationDto>
    {
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int Id { get; set; }
        //===================================================

        public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, Dtos.UserInformationDto>
        {
            //===================================================
            private readonly IApplicationDbContext _context;
            private readonly HttpClient _httpClient;

            //===================================================
            public GetUserByIdQueryHandler(IApplicationDbContext context, HttpClient httpClient)
            {
                _context = context;
                _httpClient = httpClient;
            }
            //===================================================
            public async Task<Dtos.UserInformationDto> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
            {
                //===================================================
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
                if (user == null) return null;
                //===================================================
                // میبایست آدرس سرویس ها را تا جای ممکن در فایل کانفیگ ذخیره و نگهداری نمود
                var result = await _httpClient.GetAsync($"http://localhost:48002/User/{user.Id}");
                if (result.IsSuccessStatusCode == false) return null;
                var defaultJsonSerializerOptions  = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
                var responseString = result.Content.ReadAsStringAsync().Result;
                var response = JsonSerializer.Deserialize<Dtos.UserProfileDto>(responseString, defaultJsonSerializerOptions);

                var userInfo = new Dtos.UserInformationDto
                {
                    FirstName = response.FirstName,
                    LastName = response.LastName,
                    Address = response.Address,
                    MobileNumber = user.MobileNumber,
                    Email = user.Email,
                    Id = user.Id
                };
                //===================================================
                return userInfo;
            }
        }
    }
}
