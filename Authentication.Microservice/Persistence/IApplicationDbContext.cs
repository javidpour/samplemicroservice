﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Persistence
{
    public interface IApplicationDbContext
    {
        //===================================================
        Task<int> SaveChanges();
        //===================================================
        DbSet<Domain.User> Users { get; set; }
        //===================================================
    }
}