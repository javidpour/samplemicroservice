﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain;

namespace Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        //===================================================
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            // ساخت دیتابیس و عدم استفاده از 
            // migration
            // در این مرحله
            Database.EnsureCreated();
        }
        //===================================================
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // اعمال تنظیمات موجودیت کاربر
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly);
            base.OnModelCreating(modelBuilder);
        }
        //===================================================
        public async Task<int> SaveChanges() => await base.SaveChangesAsync();
        //===================================================
        /// <summary>
        /// کابران
        /// </summary>
        public DbSet<User> Users { get; set; }
        //===================================================
    }
}
