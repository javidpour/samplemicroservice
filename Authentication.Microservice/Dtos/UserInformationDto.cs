﻿namespace Dtos
{
    public class UserInformationDto
    {
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int Id { get; set; }
        //===================================================
        /// <summary>
        /// شماره تلفن همراه
        /// </summary>
        public string MobileNumber { get; set; }
        //===================================================
        /// <summary>
        /// پست الکترونیکی
        /// </summary>
        public string Email { get; set; }
        //===================================================
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }
        //===================================================
        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        //===================================================
        /// <summary>
        /// آدرس
        /// </summary>
        public string Address { get; set; }
        //===================================================
    }
}
