﻿namespace Dtos
{
    public class UserProfileDto
    {
        //===================================================
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }
        //===================================================
        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        //===================================================
        /// <summary>
        /// آدرس
        /// </summary>
        public string Address { get; set; }
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int UserId { get; set; }
        //===================================================
    }
}
