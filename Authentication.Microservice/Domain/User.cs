﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain
{
    /// <summary>
    /// موجودیت کاربر
    /// </summary>
    public class User
    {
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int Id { get; set; }
        //===================================================
        /// <summary>
        /// شماره تلفن همراه
        /// </summary>
        public string MobileNumber { get; set; }
        //===================================================
        /// <summary>
        /// پست الکترونیکی
        /// </summary>
        public string Email { get; set; }
        //===================================================
    }

    /// <summary>
    /// تنظیمات موجودیت کاربر
    /// </summary>
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //===================================================
            /// اعمال تنظیمات موجودیت کاربر
            /// جهت منحصر بفرد شدن فیلد شماره همراه
            /// و اجباری شدن آن
            /// همچنین اعمال محدودیت بر روی طول شماره همراه
            builder.Property(a => a.MobileNumber).IsRequired().HasMaxLength(11);
            builder.HasIndex(a => a.MobileNumber).IsUnique();
            //===================================================
        }
    }
}
