﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.UserServices.Commands;
using Services.UserServices.Queries;
using System.Threading.Tasks;

namespace Authentication.Microservice.Controllers.v1
{
    [Route("[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        //===================================================
        private IMediator _mediator;
        //===================================================
        protected IMediator Mediator => _mediator ?? (HttpContext.RequestServices.GetService(typeof(IMediator)) as IMediator);
        //===================================================
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Create(CreateUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        //===================================================
        [HttpGet]
        [Route("GetUserInfo/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetUserByIdQuery { Id = id }));
        }
    }
}
