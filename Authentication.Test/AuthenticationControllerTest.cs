using Authentication.Microservice;
using Dtos;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace AuthenticationService.Test.Controllers
{
    public class AuthenticationControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> factory;

        public AuthenticationControllerTest(WebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task GetById_ReturnsJsonResult_WithUserId()
        {
            var client = factory.CreateClient();

            var response = await client.DoGetAsync<UserInformationDto>("/authentication/GetUserInfo/1");
            
            True(response != null);
        }
    }
}
