﻿using MediatR;
using Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Services.UserServices.Commands
{
    public class CreateUserProfileCommand : IRequest<int>
    {
        //===================================================
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }
        //===================================================
        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        //===================================================
        /// <summary>
        /// آدرس
        /// </summary>
        public string Address { get; set; }
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int UserId { get; set; }
        //===================================================

        public class CreateUserProfileCommandHandler : IRequestHandler<CreateUserProfileCommand, int>
        {
            //===================================================
            private readonly IApplicationDbContext _context;
            //===================================================
            public CreateUserProfileCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            //===================================================
            public async Task<int> Handle(CreateUserProfileCommand request, CancellationToken cancellationToken)
            {
                //===================================================
                // بهتر است در این موارد از
                // automapper
                // استفاده گردد
                var userProfile = new Domain.UserProfile
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Address = request.Address,
                    UserId = request.UserId,
                };
                //===================================================
                _context.UserProfiles.Add(userProfile);
                await _context.SaveChanges();
                //===================================================
                return userProfile.Id;
            }
        }
    }
}
