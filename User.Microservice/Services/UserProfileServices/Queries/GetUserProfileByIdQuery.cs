﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Services.UserServices.Queries
{
    public class GetUserProfileByIdQuery : IRequest<Domain.UserProfile>
    {
        //===================================================
        /// <summary>
        /// شناسه پروفایل کاربر
        /// </summary>
        public int Id { get; set; }
        //===================================================
        public class GetUserProfileByIdQueryHandler : IRequestHandler<GetUserProfileByIdQuery, Domain.UserProfile>
        {
            //===================================================
            private readonly IApplicationDbContext _context;
            //===================================================
            public GetUserProfileByIdQueryHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            //===================================================
            public async Task<Domain.UserProfile> Handle(GetUserProfileByIdQuery request, CancellationToken cancellationToken)
            {
                //===================================================
                var user = await _context.UserProfiles.FirstOrDefaultAsync(x=>x.UserId == request.Id,cancellationToken);
                if (user == null) return null;
                //===================================================
                return user;
                //===================================================
            }
        }
    }
}
