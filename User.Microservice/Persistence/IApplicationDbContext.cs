﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Persistence
{
    public interface IApplicationDbContext
    {
        //===================================================
        DbSet<Domain.UserProfile> UserProfiles { get; set; }
        //===================================================
        Task<int> SaveChanges();
        //===================================================
    }
}