﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain;

namespace Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        //===================================================
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            // ساخت دیتابیس و عدم استفاده از 
            // migration
            // در این مرحله
            Database.EnsureCreated();
        }
        //===================================================
        public async Task<int> SaveChanges() => await base.SaveChangesAsync();
        //===================================================
        /// <summary>
        /// پروفایل کاربران
        /// </summary>
        public DbSet<Domain.UserProfile> UserProfiles { get; set; }
        //===================================================
    }
}
