﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Services.UserServices.Commands;
using Services.UserServices.Queries;

namespace User.Microservice.Controllers.v1
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //===================================================
        private IMediator _mediator;
        //===================================================
        protected IMediator Mediator => _mediator ?? (HttpContext.RequestServices.GetService(typeof(IMediator)) as IMediator);
        //===================================================
        /// <summary>
        /// سرویس ساخت یک پروفایل کاربر جدید
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserProfileCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        //===================================================
        /// <summary>
        /// سرویس اخذ مشخصات پروفایل کاربر توسط شناسه کاربر
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetUserProfileByIdQuery { Id = id }));
        }
        //===================================================
    }
}
