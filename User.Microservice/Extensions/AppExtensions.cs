﻿using Microsoft.AspNetCore.Builder;

namespace Extensions
{
    public static class AppExtensions
    {
        /// <summary>
        /// اعمال کانفیگ های
        /// Swagger
        /// </summary>
        /// <param name="app"></param>
        public static void UseCustomSwagger(this IApplicationBuilder app)
        {
            //===================================================
            app.UseSwagger();
            //===================================================
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "doc v1");
            });
            //===================================================
        }
    }
}
