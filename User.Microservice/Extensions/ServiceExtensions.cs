﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Persistence;
using System.Reflection;

namespace Extensions
{
    public static class ServiceExtensions
    {
        //===================================================
        /// <summary>
        /// اعمال کانفیگ های
        /// Swagger
        /// </summary>
        /// <param name="services"></param>
        public static void AddCustomSwagger(this IServiceCollection services)
        {
            //===================================================
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "User.Microservice v1",
                    Description = "سرویس پروفایل کاربر",
                    Contact = new OpenApiContact
                    {
                        Name = "Farzad Javidpour",
                        Email = "javidpour.info@gmail.com"
                    }
                });
            });
            //===================================================
        }
        //===================================================
        /// <summary>
        /// اعمال کانفیگ های لایه
        /// DAL
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            //===================================================
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultSqlConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
            //===================================================
            services.AddScoped<IApplicationDbContext, ApplicationDbContext>();
            //===================================================
        }
        //===================================================
        /// <summary>
        /// اعمال کانفیگ های
        /// MediatR
        /// </summary>
        /// <param name="services"></param>
        public static void AddCustomMediatR(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }
        //===================================================
    }
}
