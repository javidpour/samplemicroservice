﻿namespace Domain
{
    /// <summary>
    /// موجودیت پروفایل کاربر
    /// </summary>
    public class UserProfile
    {
        //===================================================
        /// <summary>
        /// شناسه پروفایل کاربر
        /// </summary>
        public int Id { get; set; }
        //===================================================
        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }
        //===================================================
        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }
        //===================================================
        /// <summary>
        /// آدرس
        /// </summary>
        public string Address { get; set; }
        //===================================================
        /// <summary>
        /// شناسه کاربر
        /// </summary>
        public int UserId { get; set; }
        //===================================================
    }
}
